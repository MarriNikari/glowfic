Issues can now be found in the [issue tracker](https://bitbucket.org/Czynski/glowfic/issues?status=new&status=open).

Notes
=====
Forum: [Link](https://github.com/rubysherpas/forem/blob/e6273a8f15fb6573a743c7a71284a184647988fa/app/models/forem/concerns/viewable.rb)  
Auth: [Link](https://github.com/plataformatec/devise)  
Caching: [Link](http://guides.rubyonrails.org/caching_with_rails.html)  
More auth: [Link](https://www.railstutorial.org/book/log_in_log_out)  
